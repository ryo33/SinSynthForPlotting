# SinSynthForPlotting

## 使い方
(1)python3でssfp.pyを実行する  
(2)`frequency and amplitude > `で周波数と振幅をスペースで区切って一組ずつ入力する  
最後は何も入力せずにエンターを押す  
(3)`number of periods > `で(2)で入力した最低の周波数において何周期分出力するかを入力する  
(4)`number of division > `で何分割するかを入力する  
(5)`display mode(0 - 3) > `でディスプレイモードを入力する  
### 各表示モード
#### 0: 値を改行区切りで出力  
`ratio > `は値の倍率を入力する  
`number of franctional part > `は小数点以下何桁目まで表示するかを入力する（四捨五入）  
#### 1: 値をカンマ区切りで出力  
`ratio > `は値の倍率を入力する  
#### 2: 値をグラフにする時の概形を出力  
`width > `はグラフに使う横幅を入力する  
